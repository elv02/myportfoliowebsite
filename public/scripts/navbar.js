// Setup button event listeners after page load
document.addEventListener('DOMContentLoaded', function() {
    let buttonList = document.querySelectorAll("#navbar a");

    for(let button of buttonList){
        button.addEventListener("click", event => {
            for(let b of buttonList){
                b.classList.remove("active");
            }
            event.target.classList.add("active");
        });
    }
 }, false);